#include <iostream>
using namespace std;

void Nhap(int** a, int m, int n) {
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			cout << "a[" << i << "][" << j << "]=";
			cin >> *(*(a + i) + j);
		}
	}
}
void Xuat(int** a, int m, int n) {
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			cout << *(*(a + i) + j) << " ";
		}
		cout << endl;
	}
}
int main() {
	int row, column;
	cout << "Nhap so hang: ";
	cin >> row;
	cout << "Nhap so cot: ";
	cin >> column;
	int** a = new int* [row];
	for (int i = 0; i < column; i++) {
		*(a + i) = new int[column];
	}
	Nhap(a, row, column);
	Xuat(a, row, column);
	return 0;
}