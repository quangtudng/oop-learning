﻿#pragma once
#include "Point2D.h"
class Point3D :
	public Point2D
{
private:
	int zVal; // Thuộc tính đặc trưng của lớp con Point3D
public:
	Point3D(); // Hàm dựng không có tham số truyền vào
	Point3D(int); // Hàm dựng có 1 tham số zVal truyền vào
	Point3D(int, int, int); // Hàm dựng đầy đủ tham số
	~Point3D();
	void Show(); // Hàm show được kế thừa và tái định nghĩa từ lớp cha
	void Display();
};
