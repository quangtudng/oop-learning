﻿#include "Point3D.h"
int main() {
	cout << "Ham dung cua cha se duoc goi truoc roi moi toi ham dung lop con" << endl;
	// Lớp cha
	Point2D p2(1, 2);
	
	// Lớp con
	Point3D p3(3, 4, 5);
	// Upcast 
	cout << "_______________________________________________________" << endl;
	cout << "Upcast , con tro kieu du lieu cha dang tro ve con: " << endl << endl;
	Point2D* pointer2D = &p3;
	pointer2D->Show();
	// Slicing
	cout << "_______________________________________________________" << endl;
	cout << "Slicing, gan lop con vao lop cha khien cho bi mat du lieu: " << endl << endl;
	Point2D p4;
	p4 = p3;
	
	p4.Show();
	// Downcast
	cout << "_______________________________________________________" << endl;
	cout << "Downcast, con tro kieu du lieu con khong the tro ve cha" << endl << endl;
	cout << "Ta phai ep kieu lop con thanh lop cha , dieu nay lam mat du lieu z" << endl;
	Point3D* pointer3D = static_cast<Point3D*>(&p2);
	pointer3D->Show();
	cout << "_______________________________________________________" << endl;
	cout << "Ham huy cua con se duoc goi truoc ,roi moi toi ham huy cua cha" << endl;
	return 0;
}
// Kế thừa:
// - Access Modifier :
//    1) Kế thừa public : Thuộc tính protected và private của cha sẽ thành protected và private của con
//    2) Kế thừa private: Các thuộc tính kế thừa không bị truy xuất bởi ai ngoài trừ hàm của class đó
//    3) Kế thừa protected: Các thuộc tính kế thừa chỉ bị truy xuất bởi các hàm của lớp đó và lớp dẫn xuất của hàm đó!
// - Trong khi khởi tạo đối tượng, hàm dựng của lớp cha sẽ được gọi trước.
// - Trong khi hủy bỏ đối tượng, hàm hủy của lớp con sẽ được gọi trước
// - Upcast : Cha có thể trỏ tới con, Tuy nhiên nó không thể gọi được các method đặc trưng của lớp con
// - Downcast : Con không thể trỏ về lớp cha trừ khi sử dụng static_cast để ép kiểu
// - Slicing : Khi thực hiện phép gán từ lớp con vào lớp cha, phần dữ liệu đặc trưng bị thừa của lớp con sẽ bị cắt