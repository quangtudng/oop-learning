﻿#include "Point3D.h"
// Gọi hàm dựng lớp cha chỉ có thể thông qua danh sách khởi tạo thành viên!
// Các thuộc tính kế thừa chỉ có thể khởi tạo thông qua hàm dựng
Point3D::Point3D() // Hàm dựng không có tham số truyền vào
	: Point2D(), zVal(3)
{
	cout << "Ham dung khong co tham so truyen vao cua Point3D duoc goi" << endl;
}
Point3D::Point3D(int z) // Hàm dựng có 1 tham số zVal truyền vào
	: Point2D(), zVal(z)
{
	cout << "Ham dung cua lop con Point3D chi truyen vao zVal duoc goi" << endl;
}
Point3D::Point3D(int x=1, int y=2, int z=3) // Hàm dựng có đầy đủ tham số truyền vào
	: Point2D(x, y), zVal(z)
{
	cout << "Ham dung cua lop con Point3D day du tham so duoc goi" << endl;
}
// Tái định nghĩa lại hàm Show
void Point3D::Show() {
	cout << "Ham show cua Point3D" << endl;
	cout << "Gia tri cua x la: " << this->xVal << endl << "Gia tri cua y la: " << this->yVal << endl << "Gia tri cua z la: " << this->zVal << endl;
}
void Point3D::Display() {
	cout << "Ham display dac trung cua Point3D:" << endl;
	cout << "Gia tri cua x la: " << this->xVal << endl << "Gia tri cua y la: " << this->yVal << endl << "Gia tri cua z la: " << this->zVal << endl;
}
Point3D::~Point3D() {
	cout << "Ham huy Point3D" << endl;
}