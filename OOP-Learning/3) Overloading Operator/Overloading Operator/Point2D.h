﻿#pragma once
#include <iostream>
using namespace std;
class Point2D
{
private:
	int xVal;
	int yVal;
public:
	// -- Constructor --
	// Hàm dựng mặc định với tham số sau khi đã gộp
	Point2D(int = 1, int = 2);
	// Hàm dựng được tạo với mục đích chuyển đổi tham số chương trình định nghĩa thành tham số do user định nghĩa
	Point2D(int&);
	// Hàm dựng sao chép, phép gán có thể coi là hàm dựng sao chép , nên không cần đa năng hóa phép gán
	Point2D(const Point2D&);
	// -- Destructor --
	~Point2D();
	// -- Other methods --
	// Show the variables
	void Show();
	friend ostream& operator << (ostream&,const Point2D&);
	friend istream& operator >> (istream&, Point2D&);
	// Toán tử đơn hạng
	Point2D operator + (Point2D&);
	Point2D operator - (Point2D&);
	Point2D operator * (Point2D&);
	Point2D operator ! ();
	Point2D operator ~ ();
	Point2D operator & (Point2D&);
	Point2D* operator -> ();
	Point2D operator ->* (Point2D&);
};

