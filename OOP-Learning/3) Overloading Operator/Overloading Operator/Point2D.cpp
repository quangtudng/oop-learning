﻿#include "Point2D.h"
Point2D::Point2D(int xVal, int yVal) 
	: xVal(xVal), yVal(yVal) 
{

}
Point2D::Point2D(int& n) : xVal(n), yVal(n) {

}

Point2D::Point2D(const Point2D& p) {
	this->xVal = p.xVal;
	this->yVal = p.yVal;
}
// Destructor
Point2D::~Point2D() {

}
// Other methods
void Point2D::Show() {
	cout << "Ham show cua POint2D duoc goi" << endl;
	cout << "Gia tri cua x la: " << this->xVal << endl;
	cout << "Gia tri cua y la: " << this->yVal << endl;
}
ostream& operator << (ostream& output,const Point2D& Object) {
	cout << "Ham da nang hoa toan tu xuat " << endl;
	output << "Gia tri cua x la: " << Object.xVal << endl;
	output << "Gia tri cua y la: " << Object.yVal << endl;
	return output;
}
istream& operator >> (istream& input, Point2D& Object) {
	cout << "Ham da nang hoa toan tu nhap " << endl;
	cout << "Nhap vao x:";
	input >> Object.xVal;
	cout << "Nhap vao y:";
	input >> Object.yVal;
	cout << endl;
	return input;
}
Point2D Point2D::operator+ (Point2D& Object2) {
	return Point2D(this->xVal + Object2.xVal, this->yVal + Object2.yVal);
}
Point2D Point2D::operator- (Point2D& Object2) {
	return Point2D(this->xVal - Object2.xVal, this->yVal - Object2.yVal);
}
Point2D Point2D::operator* (Point2D& Object2) {
	return Point2D(this->xVal * Object2.xVal, this->yVal * Object2.yVal);
}
Point2D Point2D::operator!() {
	return Point2D(!this->xVal, !this->yVal);
}
Point2D Point2D::operator~() {
	return Point2D(~this->xVal, ~this->yVal);
}
Point2D Point2D::operator&(Point2D& Object) {
	return Point2D(Object.xVal & this->xVal, Object.yVal & this->yVal);
}
Point2D* Point2D::operator-> () {
	return this;
}
Point2D Point2D::operator ->* (Point2D& Object) {
	return Object;
}