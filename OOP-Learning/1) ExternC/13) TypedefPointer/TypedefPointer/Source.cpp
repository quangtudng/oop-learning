﻿#include <iostream>
using namespace std;
typedef int p;
typedef int* q;
int main() {
	int x = 1, y = 2;
	p* p1, * p2;
	p1 = &x;
	p2 = &y;
	cout << "Con tro p1 co gia tri: " << *p1 << endl;
	cout << "Con tro p2 co gia tri: " << *p2 << endl;
	q p3, p4;
	p3 = &x;
	p4 = &y;
	cout << "Con tro p3 co gia tri: " << *p3 << endl;
	cout << "Con tro p4 co gia tri: " << *p4 << endl;
	p* p5, p6;
	p5 = &x;
	//p6 = &y; // Lưu ý khi khai báo kiểu gộp chung như thế này , trình biên dịch sẽ hiểu sai thành int* p5 và int p6
}