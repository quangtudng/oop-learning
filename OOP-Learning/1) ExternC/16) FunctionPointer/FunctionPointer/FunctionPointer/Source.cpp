﻿#include <iostream>
using namespace std;
void Swap(int &First, int &Second) {
	int temp = First;
	First = Second;
	Second = temp;
}
bool ascending(int left, int right) {
	return left > right;
}
bool descending(int left, int right) {
	return left < right;
}
void SelectionSort(int* p, int length, bool Comp(int,int)) {
	for (int i = 0; i < length; i++) {
		for (int j = i + 1; j < length; j++) {
			if (Comp(*(p + i), *(p + j))) {
				Swap(*(p + i), *(p + j));
			}
		}
	}
}
void Show(int* p, int length) {
	for (int i = 0; i < length; i++) {
		cout << *(p + i) << " ";
	}
	cout << endl;
}
int main() {
	int A[] = { 2,6,1,88,24,22,22,67 };
	int l = sizeof(A) / sizeof(int);
	SelectionSort(A, l, ascending);
	Show(A, l);
	SelectionSort(A, l, descending);
	Show(A, l);
}
// Sử dụng con trỏ hàm làm đối số :
// Sử dụng khi cần gọi 1 hàm như là tham số của 1 hàm khác;