﻿#include <iostream>
using namespace std;
void SwapInt(int &x, int &y) {
	cout << "Thuc hien hoan doi" << endl;
	int temp = x;
	x = y;
	y = temp;
}
int main() {
	int m = 1, n = 2;
	cout << "Gia tri cua m la: " << m << endl;
	cout << "Gia tri cua n la: " << n << endl;
	SwapInt(m, n);
	cout << "Gia tri cua m la: " << m << endl;
	cout << "Gia tri cua n la: " << n << endl;
}
// THỰC HIỆN THÔNG THƯỜNG
//		1) Khi thực hiện gọi hàm SwapInt ,ta truyền vào hai biến số m và n.
//		2) Khi chạy vào hàm nó sẽ tạo ra hai bản copy hai số m và n sau đó gán cho x và y
//		3) Khi thực hiện xong hàm x và y sẽ được giải phóng và nó không ảnh hưởng gì đến m và n
// THỰC HIỆN THÔNG QUA BIẾN THAM CHIẾU
//		1) Khi thực hiện gọi hàm SwapINT , ta truyền vào hai biến số m và n
//		2) Khi chạy vào hàm, x và y sẽ dùng chung vùng nhớ của m và n được truyền vào
//		3) Khi thực hiện xòng hàm , x và y lúc này do dùng chung vùng nhớ với m và n nên sẽ trực tiếp hoán đổi giá trị m và n thông qua tham chiếu