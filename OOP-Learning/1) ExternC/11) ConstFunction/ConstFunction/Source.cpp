﻿#include <iostream>
using namespace std;

const int Func() {
	int x = 4;
	x++;
	return x;
}
int main() {
	int x = Func();
	//int y = Func()++; // Báo lỗi biên dịch
}
// Hàm hằng là hàm không cho phép thay đổi giá trị trả về của nó
// Sự khác nhau giữa hàm hằng const int và int const là
//		1) const int là kiểu hàm hằng dành cho hàm bình thường
//		2) int const là kiểu hàm hằng dành cho hàm thành viên , được sử dụng trên các hàm getter khi bạn không muốn thay đổi thuộc tính class