﻿#include <iostream>
using namespace std;
void Nhap(int** p, int m, int n) {
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			cout << "p[" << i << "][" << j << "]=";
			cin >> *(*(p + i) + j);
		}
	}
}
void Xuat(int** p, int m, int n) {
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			cout << *(*(p + i) + j);
		}
		cout << endl;
	}
}
int main() {
	int hang, cot;
	cout << "Nhap vao so hang: ";
	cin >> hang;
	cout << endl;
	cout << "Nhap vao so cot: ";
	cin >> cot;
	int** p = new int*[hang];
	for (int i = 0; i < hang; i++) {
		*(p + i) = new int[cot];
	}
	Nhap(p, hang, cot);
	Xuat(p, hang, cot);
	for (int i = 0; i < hang; i++) {
		delete[]p[i];
	}
	delete []p;
	return 0;
} 
// Ý tưởng : Việc cấp phát động cho một mảng hai chiều có thể chia thành các bước như sau
//		Bước 1 : Nhập vào số hàng và cột
//		Bước 2 : Cấp phát động một mảng các con trỏ, có thể hiểu đây là tạo ra một hàng
//		Bước 3 : Trên mỗi hàng, ta tiếp tục cấp phát động một mảng con trỏ với số cột tương ứng , đây là tạo ra các cột trên mỗi hàng
// Lưu ý : Để lấy giá trị , ta phải xài 2 lần con trỏ :
//		*(a+i)+j : Địa chỉ tại dòng i cột j
//		*(a+i)	: Địa chỉ tại dòng i
//		*(*(a+i)+j) : Giá trị tại dòng i cột j