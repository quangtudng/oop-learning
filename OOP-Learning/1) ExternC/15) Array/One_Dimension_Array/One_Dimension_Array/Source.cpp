#include <iostream>
using namespace std;
void Nhap(int *p, int n) {
	for (int i = 0; i < n; i++) {
		cout << "p[" << i << "]=";
		cin >> *(p + i);
	}
}
void Xuat(int *p, int n) {
	cout << "Phan tu co trong mang: " << endl;
	for (int i = 0; i < n; i++) {
		cout << "p[" << i << "]=";
		cout << *(p + i) << endl;
	}
}
int main() {
	int n;
	cout << "Nhap vao so phan tu cua mang mot chieu nay: ";
	cin >> n;
	int* p = new int[n];
	Nhap(p, n);
	Xuat(p, n);
	return 0;
}