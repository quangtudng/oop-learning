﻿#include <iostream>
using namespace std;
void Show(int m) {
	m++;
	cout << "Gia tri cua m++ la :" << m << endl;
}
void ShowWithReference(int &m) {
	m++;
	cout << "Gia tri cua m++ la :" << m << endl;
}
void ShowWithConst(const int m) {
	//m++; // Báo lỗi biên dịch
	cout << "Gia tri cua m++ la :" << m << endl;
}
int main() {
	int x = 1;
	const int y = 1;
	Show(x); // Kết quả : 2
	Show(y); // Kết quả : 2
	ShowWithReference(x); // Kết quả : 2
	//ShowWithReference(y);  // Báo lỗi biên dịch
}
// HÀM SHOW
//		Hàm show ở cả hai trường hợp đều chạy tốt vì lý do là khi ta truyền x y vào ta đang lấy bản copy của nó để thay đổi nên sẽ không ảnh hưởng
// HÀM SHOWWITHREFERENCE
//		Hàm show này sẽ báo lỗi khi truyền y vào vì y được khai báo là một biến hằng
// HÀM SHOWWITHCONST
//		Hàm show này sẽ báo lỗi biên dịch ngay trong hàm vì danh sách biến truyền vào kiểu dữ liệu là biến hằng