﻿#include<iostream>
using namespace std;

int main() {
	int x = 10;
	int y = 100;
	const int* z = &x; // Con trỏ hằng: Không thể thay đổi giá trị nhưng có thể thay đổi được vùng nhớ nó trỏ tới
	//int const* z = &x; // Giống hết const int , con trỏ hằng
	cout <<  "Z la mot con tro hang  ( a pointer to int constant ):" << endl;
	cout << "====================================================" << endl;
	cout << "Address: " << z << " . Value: " << *z << endl;
	cout << "Thay doi vung nho nhung khong thay doi gia tri" << endl;
	//*z = 6969; // Lỗi biên dịch
	z = &y; 
	cout << "Address: " << z << " . Value: " << *z << endl;
	int* const t = &x; // Hằng con trỏ: Có thể thay đổi giá trị nhưng không thể thay đổi vùng nhớ nó trỏ tới
	cout << endl << "T la mot hang con tro ( a pointer constant to int )" << endl;
	cout << "====================================================" << endl;
	cout << "Address: " << t << " . Value: " << *t << endl;
	//t = &y;  // Lỗi biên dịch
	cout << "Thay doi gia tri nhung khong thay doi vung nho" << endl;
	*t = 6969;
	cout << "Address: " << t << " . Value: " << *t << endl;
	cout << endl << "M la mot hang con tro hang  ( a constant pointer to constant int )" << endl;
	cout << "====================================================" << endl;
	int const* const m = &x; // Không thể thay đổi giá trị và vùng nhớ nó trỏ tới
	cout << "Khong thay doi duoc gia tri va vung nho" << endl;
	//m = &y; // Lỗi biên dịch
	//*m = y; // Lỗi biên dịch
	return 0;
}
// Con trỏ hằng : Thay đổi được vùng nhớ nhưng không thay đổi được giá trị
// Hằng con trỏ : Thay đổi được giá trị nhưng không thay đổi được vùng nhớ
// Hằng con trỏ hằng : Không thay đổi được cả giá trị và vùng nhớ