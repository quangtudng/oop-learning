﻿#include <iostream>
using namespace std;

int main() {
	cout << "BIEN THAM CHIEU " << endl;
	cout << "================================" << endl;
	int x = 5;
	int& y = x;
	y = 1;
	cout << "Gia tri cua x la : " << x << endl;
	cout << "Gia tri cua y dang tham chieu toi y la : " << y << endl;
	x++;
	cout << "Thay doi gia tri x..." << endl;
	cout << "Gia tri cua x la : " << x << endl;
	cout << "Gia tri cua y dang tham chieu toi y la : " << y << endl;
	return 0;
}
// I) Biến tham chiếu :
//		1) Biến tham chiếu là một tên khác cho biến đã định nghĩa
//		2) Biến tham chiếu sẽ sử dụng chung vùng nhớ của biến mà nó đang tham chiếu tới nên nó sẽ không được cấp phát vùng nhớ
//		3) Biến tham chiếu có thể được tham chiếu tới mảng nhưng không được tạo mảng tham chiếu
//		4) Lưu ý khi khai báo phải chỉ rõ nó đang tham chiếu tới biến nào
// II) Hằng tham chiếu :
//		1) Hằng tham chiếu có cách hoạt động tương tự như biến tham chiếu bình thường
//		2) Khi sử dụng hằng tham chiếu, ta chỉ có thể lấy ra giá trị nơi nó tham chiếu tới chứ không được đổi giá trị