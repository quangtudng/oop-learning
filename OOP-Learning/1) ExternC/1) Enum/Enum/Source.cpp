﻿#include <iostream>
using namespace std;
int main() {
	enum Color {
		RED = 1,
		BLUE = 2,
		GREEN = 'A',
		YELLOW = 4,
	};
	Color c = GREEN;
	cout << c << endl;
	system("PAUSE");
	return 0;
}
// 1) Khai báo Enum không cấp phát vùng nhớ, chỉ cấp phát bộ nhớ lúc tạo biến kiểu enum
// 2) Mặc định các phần tử của Enum là kiểu dữ liệu Int
// 3) Enum sẽ tự ép kiểu ngầm định sang kiểu int nhưng kiểu int phải ép kiểu tường minh sang enum