﻿#include<iostream>
using namespace std;
void Show(int x, int y = 1, int z = 2);
int main() {
	//Show(); // Báo lỗi biên dịch vì truyền vào không đủ tham số
	cout << "Ham show co gia tri x truyen vao la 69 " << endl;
	Show(69); // Chạy bình thường vì đã có 2 tham số truyền vào 
	cout << "Ham show co gia tri x truyen vao la 69 va y la 70" << endl;
	Show(69, 70);
	cout << "Ham show co gia tri x truyen vao la 69 va y la 70 va z la 71" << endl;
	Show(69, 70, 71);
	//Show(1, , 3); // Báo lỗi biên dịch vì tham số truyền vào không có gì
}
void Show(int x, int y, int z) {
	cout << "Gia tri cua x la : " << x << endl;
	cout << "Gia tri cua y la : " << y << endl;
	cout << "Gia tri cua z la : " << z << endl;
}
//  ĐỐI SỐ MẶC ĐỊNH
//  1) Định nghĩa các giá trị tham số mặc định cho các hàm;
//  2) Các đối số mặc định cần là các đối số cuối cùng tính từ trái qua phải;
//  3) Nếu chương trình sử dụng khai báo nguyên mẫu hàm thì các đối số mặc định càn được khởi gán trong nguyên mẫu hàm
//  4) Không được khởi gán lại cho các đối số mặc định trong dòng đầu của định nghĩa hàm;