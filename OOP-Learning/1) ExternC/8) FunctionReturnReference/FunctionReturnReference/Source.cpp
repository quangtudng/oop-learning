﻿#include<iostream>
using namespace std;
int x = 1;
int& FuncOne() {
    return x;
}
int& FuncTwo() {
    int x = 2;
    return x; // Dù vùng nhớ đã bị thu hồi , nó vẫn trỏ tới vùng nhớ đã được giải phóng
}
int& FuncThree() {
    static int x = 3;
    return x;
}
int main() {
    cout << "Truong hop la bien toan cuc" << endl;
    cout << "==================================" << endl;
    int* p1 = &FuncOne(); // Con trỏ vẫn sẽ trỏ đúng
    cout << "Vung nho con tro p1 co gia tri: " << *p1 << endl; // 1
    cout << "Vung nho con tro p1 co gia tri: " << *p1 << endl; // 1
    cout << endl << "Truong hop la bien cuc bo" << endl;
    cout << "==================================" << endl;
    int* p2= &FuncTwo(); // Con trỏ chỉ trỏ đúng một lần , cho đến khi vùng nhớ biến cục bộ bị reset
    cout << "Vung nho con tro p2 co gia tri: " << *p2 << endl; // 2
    cout << "Vung nho con tro p2 co gia tri: " << *p2 << endl; // giá trị măc định - Sau lần thứ nhất , con trỏ trỏ vùng nhớ của Func đã bị reset
    cout << endl << "Truong hop la bien cuc bo tinh" << endl;
    cout << "==================================" << endl;
    int* p3 = &FuncThree(); // Con trỏ vẫn sẽ trỏ đúng
    cout << "Vung nho con tro p3 co gia tri: " << *p3 << endl; // 3
    cout << "Vung nho con tro p3 co gia tri: " << *p3 << endl; // 3
    return 0;
}
// Khi giá trị trả về của hàm là tham chiếu, câu lệnh gán hơi khác thường, trong đó vế trái là lời gọi hàm chứ không phải là tên biến
// TRƯỜNG HƠP 1 : FUNCTION TRẢ VỀ MỘT BIẾN TOÀN CỤC
//      Ta có thể sử dụng được giá trị của hàm bình thường
// TRƯỜNG HƠP 2 : FUNCTION TRẢ VỀ MỘT BIẾN CỤC BỘ
//      Nếu trả về tham chiếu đến một biến cục bộ thì biến cục bộ này sẽ bị mất đi khi kết thúc thực hiện hàm.
// TRƯỜNG HƠP 3 : FUNCTION TRẢ VỀ MỘT BIẾN CỤC BỘ TĨNH
//      Khi ta khai báo biến cục bộ tĩnh , các biến này sẽ không được giải phóng khi kết thúc thực hiện hàm ,tương tự như biến cục bộ