﻿#include <iostream> 
using namespace std;

void Show(char* str) {
	cout << str;
}
int main() {
	const char* str = "DUT";
	// Show(str);
	Show(const_cast<char*>(str));
}
// Ta có thể sử dụng const_cast để loại bỏ const khỏi biến