﻿#include <iostream>
#include <iomanip>
using namespace std;

int main() {
	cout << setw(10) << "TestSetW";
	return 0;
}
// Để quy định độ rộng tối thiểu để hiển thị là k vị trí cho giá trị
//Hàm này cần đặt trong toán tử xuất và nó chỉ có hiệu lực cho một giá trị được in gần nhất.
//Các giá trị in ra tiếp theo sẽ có độ rộng tối thiểu mặc định là 0