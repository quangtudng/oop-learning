﻿#include<iostream>
using namespace std;
int x = 1;
int& FuncOne() {
    return x;
}
int& FuncTwo() {
    int x = 2; // Cấp phát vùng nhớ cho x cục bộ
    return x; // Tại đây sau khi trả về x cục bộ đã bị giải phóng
}
int& FuncThree() {
    static int x = 3;
    return x; // Tại đây sau khi trả về x cục bộ tĩnh thì nó chưa bị giải phóng
}
const int& FuncFour() {
    static int x = 4;
    return x; // Tại đây sau khi trả về x cục bộ tĩnh thì nó chưa bị giải phóng
}
int main() {
    cout << "Truong hop la bien toan cuc" << endl;
    cout << "==================================" << endl;
    cout << "Vung nho FuncOne co gia tri: " << ++FuncOne() << endl; // 2
    cout << "Vung nho FuncOne co gia tri: " << ++FuncOne() << endl; // 3
    cout << endl << "Truong hop la bien cuc bo" << endl;
    cout << "==================================" << endl;
    cout << "Vung nho FuncTwo co gia tri: " << ++FuncTwo() << endl; // 3
    cout << "Vung nho FuncTwo co gia tri: " << ++FuncTwo() << endl; // 3 - Giá trị không đổi vì nó đang sử dụng lại biến cục bộ động
    cout << endl << "Truong hop la bien cuc bo tinh" << endl;
    cout << "==================================" << endl;
    cout << "Vung nho FuncThree co gia tri: " << ++FuncThree() << endl; // 4
    cout << "Vung nho FuncThree co gia tri: " << ++FuncThree() << endl; // 5
    cout << endl << "Truong hop la ham tra ve hang tham chieu" << endl;
    cout << "==================================" << endl;
    cout << "Vung nho FuncFour co gia tri: " << FuncFour() << endl; // 4
    //cout << "Vung nho FuncFour co gia tri: " << ++FuncFour() << endl; // Lỗi biên dịch vì hàm trả về hằng tham chiếu, không thể thay đổi được giá trị
    return 0;
}
// Hàm trả về tham chiếu là một hàm sử dụng chung vùng nhớ với giá trị mà nó trả về
// Khi giá trị trả về của hàm là tham chiếu, câu lệnh gán hơi khác thường, trong đó vế trái là lời gọi hàm chứ không phải là tên biến
// TRƯỜNG HƠP 1 : FUNCTION TRẢ VỀ MỘT BIẾN TOÀN CỤC
//      Khi ta sử dụng biến cục bộ giá trị của nó khi thay đổi thông qua hàm trả về tham chiếu có thể thay đổi được
// TRƯỜNG HƠP 2 : FUNCTION TRẢ VỀ MỘT BIẾN CỤC BỘ
//      Nếu trả về tham chiếu đến một biến cục bộ thì biến cục bộ này sẽ bị mất đi khi kết thúc thực hiện hàm.
// TRƯỜNG HƠP 3 : FUNCTION TRẢ VỀ MỘT BIẾN CỤC BỘ TĨNH
//      Khi ta khai báo biến cục bộ tĩnh , các biến này sẽ không được giải phóng khi kết thúc thực hiện hàm ,tương tự như biến toàn cục