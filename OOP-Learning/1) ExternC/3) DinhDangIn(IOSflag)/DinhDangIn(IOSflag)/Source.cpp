﻿#include <iostream>
#include <iomanip>
using namespace std;

int main() {
	double x1 = 123.5;
	double x2 = 1234.5;
	double x3 = 12.5;
	cout << setiosflags(ios::showpoint) << setprecision(2);
	cout << x1 << endl;
	cout << x2 << endl;
	cout << x3 << endl;
	return 0;
}
// Quy định số thực được hiển thị ra màn hình với p chữ số 
// setiosflags là để bật cờ hiệu cho ios::showpoint
// Khi quy định số thực in ra màn hình bằng với số phần nguyên, nếu nó có thể làm tròn được , kết quả in ra sẽ là một số đã làm tròn