﻿#pragma once
#include <iostream>
using namespace std;
class Point2D
{
protected:
	int xVal;
	int yVal;
public:
	Point2D(int = 6, int = 9);
	virtual ~Point2D();
	// Nếu không phải là hàm ảo , chương trình sẽ liên kết lời gọi phương thức với định nghĩa hàm
	// void Show();

	// Liên kết tĩnh : ta biến nó thành hàm ảo để cắt cắt sợi dây liên kết
	virtual void Show();
	//virtual void Show() = 0;
};
