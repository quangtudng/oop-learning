﻿#pragma once
#include "Point2D.h"
class Point3D :
	public Point2D
{
protected:
	int zVal;
public:
	Point3D(int);
	Point3D(int, int, int);
	virtual ~Point3D();
	void Display();
	// Cứ thêm vào virtual ở phương thức con , không thừa
	virtual void Show();
};
