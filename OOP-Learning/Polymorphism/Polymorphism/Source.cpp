﻿#include "Point3D.h"
int main() {
	cout << "___________________________________________________" << endl;
	Point2D* p2 = new Point3D(1,2,3);
	cout << "Da hinh tinh, chuong trinh goi ham Show kieu du lieu con tro Point2D thay vi doi tuong Point3D no dang tro toi " << endl << endl;
	cout << "Sau khi them tu khoa virtual de bien ham Show cua lop co so thanh ham ao , no se chay dung" << endl;
	p2->Show();
	cout << "___________________________________________________" << endl;
	cout << "Destructor nen de la ham ao ly do la khi co con tro, ta can giai phong dung vung nho" << endl;
	cout << "Neu khong la ham ao, doi tuong Point3D tao ra boi tu khoa new se khong duoc giai phong" << endl;
	delete p2;
	return 0;
}
// Static Binding:
// Đây được gọi là liên kết tĩnh , nó sẽ ưu tiên gọi hàm kiểu dữ liệu con trỏ và không quan tâm đến kiểu dữ liệu lớp con
// Đây là đa hình tĩnh , kiểu đa hình được cài đặt do liên kết tĩnh
// Điều này là điều không logic! Phải cắt liên kết của con trỏ này về hàm của kiểu dữ liệu con trỏ 
// Giải pháp là : Chỉ xây một liên kết di động khi gọi hàm ( LIÊN KẾT ĐỘNG - ĐA HÌNH ĐỘNG )
// Virtual:
// Ta sẽ sử dụng hàm/phương thức ảo  - Virtual để cài đặt đa hình động
// Ta sẽ bảo chương trình sử dụng liên kết động thay cho liên kết tĩnh
// Yêu cầu chương trình dừng liên kết lời gọi phương thức với định nghĩa hàm cho đến khi chương trình chạy
// Lưu ý :
// Nếu khi đa hình thì Overload sẽ không kế thừa các method đã được overload mà chỉ kế thừa lại hàm mà mình vừa đa hình
// Constructor không thể khai báo hàm ảo
// Destructor nên khai báo là hàm ảo