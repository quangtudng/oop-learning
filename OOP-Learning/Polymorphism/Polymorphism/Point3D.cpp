﻿#include "Point3D.h"
// Gọi hàm dựng lớp cha chỉ có thể thông qua danh sách khởi tạo thành viên!
// Các thuộc tính kế thừa chỉ có thể khởi tạo thông qua hàm dựng
Point3D::Point3D(int x, int y, int z)
	: Point2D(x, y), zVal(z)
{
	cout << "Ham dung day du tham so cua Point3D duoc goi" << endl;
}
Point3D::Point3D(int z)
	: Point2D(), zVal(z)
{
	cout << "Ham dung chi co mot tham so z cua Point3D duoc goi" << endl;
}
// Tái định nghĩa lại hàm Show ( Overriding )
void Point3D::Show() {
	cout << "Ham show cua Point3D duoc goi: " << endl;
	cout << "Gia tri cua x la: " << this->xVal << endl;
	cout << "Gia tri cua y la: " << this->yVal << endl;
	cout << "Gia tri cua z la: " << this->zVal << endl;
}
void Point3D::Display() {
	cout << "Ham Display cua Point3D duoc goi :" << endl;
	cout << this->xVal << " " << this->yVal << " " << this->zVal;
}
Point3D::~Point3D() {
	cout << "Ham huy cua Point3D duoc goi" << endl;
}