﻿#include <iostream>
#include "Vector.cpp"
#include "Point2D.h"
// Nếu include file .h , nó không reference tới cpp
int main() {
	Point2D p1(6,9);
	Vector<Point2D>alo(p1,4);
	alo.show();
}
// Với class template ta chỉ có thể gọi cách tường minh , lý do là vì nó phải cấp phát vùng nhớ cho đối tượng