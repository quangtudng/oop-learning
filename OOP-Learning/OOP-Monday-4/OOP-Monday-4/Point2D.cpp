﻿#include "Point2D.h"
// Default constructor with variable
Point2D::Point2D(int xVal, int yVal) : xVal(xVal), yVal(yVal) {

}
Point2D::Point2D(int& n) : xVal(n), yVal(n) {

}
// Copy constructor
Point2D::Point2D(const Point2D& p) {
	this->xVal = p.xVal;
	this->yVal = p.yVal;

}
// Destructor
Point2D::~Point2D() {

}
// Other methods
void Point2D::Show() {
	cout << "Gia tri cua x la: " << this->xVal << endl << "Gia tri cua y la: " << this->yVal << endl;
}
ostream& operator<<(ostream& o, const Point2D& p) {
	o << "Gia tri cua x la: " << p.xVal << endl << "Gia tri cua y la: " << p.yVal << endl;
	return o;
}