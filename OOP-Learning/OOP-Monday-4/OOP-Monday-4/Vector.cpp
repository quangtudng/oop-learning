#include "Vector.h"
template<typename T>
Vector<T>::Vector(T t, int s) 
	:size(s)
{
	this->data = new T[this->size];
	for (int i = 0; i < this->size; i++) {
		*(this->data + i) = t;
	}
}
template<typename T>
Vector<T>::~Vector() {
	delete[] this->data;
}
template<typename T>
void Vector<T>::show()
{
	for (int i = 0; i < this->size; i++) {
		cout << *(this->data + i);
	}
}