﻿#pragma once
#include <iostream>
using namespace std;
//template<typename T>
template<class T>
// Cách 1: Không thể truyền class template vào một class template
//template<typename T, int U>
// Cách 2: Kiểu dữ liệu cụ thể , phải truyền giá trị vào cho nó 
// Cách 3: Khai báo lồng template nằm trong một template khác
class Vector
{
public:
	int size;
	T* data;
public:
	Vector(T, int = 3);
	~Vector();
	void show();
};
// Đây là một khuôn mẫu lớp

