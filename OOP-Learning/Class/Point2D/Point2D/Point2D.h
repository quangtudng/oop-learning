﻿#pragma once
#include <iostream>
using namespace std;
class Point2D
{
	int xVal;
	int yVal;
	// Biến count 
	static int count;
public:
	// -- Constructor --
	Point2D();
	// Hàm dựng mặc định với tham số sau khi đã gộp
	Point2D(int, int);
	// Hàm dựng được tạo với mục đích chuyển đổi tham số chương trình định nghĩa thành tham số do user định nghĩa
	Point2D(int);
	// Hàm dựng sao chép
	Point2D(const Point2D&);
	// -- Destructor --
	~Point2D();
	// -- Other methods --
	// Show the variables
	void Show();
	Point2D operator+(const Point2D&);
	//friend Point2D operator+(const Point2D&, const int&);
	//friend Point2D operator+(const int&, const Point2D&);
	// Show count
	static void ShowCount();
	// Friend function
	friend void Display(const Point2D&);
	//friend Point2D operator+(const Point2D&, const Point2D&);
	friend ostream& operator<<(ostream&, const Point2D&);
	friend istream& operator>>(istream&, Point2D&);
};

