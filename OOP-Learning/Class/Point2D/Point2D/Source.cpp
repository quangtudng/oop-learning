﻿#include <iostream>
#include "Point2D.h"
using namespace std;
int main() {
	// Trường hợp 1 : Đối tượng tạo ra được truyền vào không có tham số 
	cout << "Doi tuong 1 tao ra khong co tham so truyen vao: " << endl;
	Point2D p1;
	p1.Show();
	Display(p1);
	// Trường hợp 2 : Đối tượng tạo ra được truyền vào tham số
	cout << "Doi tuong 2 tao ra co tham so truyen vao: " << endl;
	Point2D p2(6, 9);
	p2.Show();
	// Trường hợp 3 : Đối tượng tạo ra được copy từ đối tượng 2
	cout << "Doi tuong 3 duoc copy tu doi tuong 2: " << endl;
	Point2D p3(p2);
	p3.Show();
	// Trường hợp 4 : Đối tượng được tạo ra và gán vào con trỏ
	cout << "Doi tuong 4 duoc tao ra va gan vao con tro: " << endl;
	Point2D* p4 = new Point2D(3, 4);
	p4->Show();
	(*p4).Show();
	// Trường hợp 5 : Đối tượng được tạo ra từ tổng của đối tượng 1 và 2 
	//cout << "Doi tuong 5 duoc tao ra tu tong cua doi tuong 1 va 2: " << endl;
	//Point2D p5 = p1+p2;
	//p5.Show();
	// Trường hợp 6 : Đối tượng được tạo ra từ tổng của đối tượng 2 và 3 ( cách sử dụng hàm global )
	//cout << "Doi tuong 6 duoc tao ra tu tong cua doi tuong 2 va 3: " << endl;
	//Point2D p6 = operator+(p2,p3);
	//p6.Show();
	// Trường hợp 7 : Đối tượng được tạo ra từ tổng của đối tượng 2 và 3 ( cách sử dụng hàm thành viên )
	cout << "Doi tuong 7 duoc tao ra tu tong cua doi tuong 2 va 3: " << endl;
	//Point2D p7 = p2.operator+(p3);
	Point2D p7 = p2 + 3;
	p7.Show();
	// Trường hợp 8 : Nhập vào xVal , yVal
	cout << "Doi tuong 8 duoc tao ra tu ban phim nhap vao: " << endl;
	Point2D p8;
	//operator>>(cin, p8);
	//operator<<(cout, p8);
	cin >> p8;
	cout << p8;
	// 
	// Chỉ có thể gọi hàm như thế này nếu hàm đó là hàm static
	Point2D::ShowCount();
	// Đối với trường hợp này, phải xóa con trỏ thì destructor mới thực thi
	delete p4;
	return 0;
}