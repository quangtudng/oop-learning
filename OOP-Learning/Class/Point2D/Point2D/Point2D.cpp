﻿#include "Point2D.h"
int Point2D::count = 0;
// Default constructor with variable
Point2D::Point2D() : xVal(1), yVal(2) {
	count++;
}
Point2D::Point2D(int xVal, int yVal) : xVal(xVal), yVal(yVal) {
	// Khi kết hợp sử dụng với danh sách khởi tạo thành viên thì nó sẽ ưu tiên phép gán
	//this->xVal = 1;
	//this->yVal = 2;
	count++;
}
Point2D::Point2D(int n) : xVal(n), yVal(n) {
	count++;
}
// Copy constructor
Point2D::Point2D(const Point2D& p) {
	this->xVal = p.xVal;
	this->yVal = p.yVal;
	Point2D::count++;
}
// Destructor
Point2D::~Point2D() {
	//cout << "Destructor has been called my dude" << endl;
	Point2D::count--;
}
// Other methods
void Point2D::Show() {
	cout << "Gia tri cua x la: " << this->xVal << endl << "Gia tri cua y la: " << this->yVal << endl;
}
void Point2D::ShowCount() {
	cout << "Ham dung duoc goi tat ca " << Point2D::count << " lan" << endl;
}
// Global Function
void Display(const Point2D& p) {
	cout << "Khi su dung ham ban" << endl << "gia tri cua x la: " << p.xVal << endl << "Gia tri cua y la: " << p.yVal << endl;
}
// Đây là trường hợp hàm đa năng hóa toán tử cộng bằng hàm thành viên
Point2D Point2D::operator+(const Point2D& p2) {
	Point2D p;
	p.xVal = this->xVal + p2.xVal;
	p.yVal = this->yVal + p2.yVal;
	return p;
}
// Đây là trường hợp hàm đa năng hóa toán tử cộng bằng hàm global
//Point2D operator+(const Point2D& p1, const Point2D& p2) {
//	Point2D p;
//	p.xVal = p1.xVal + p2.xVal;
//	p.yVal = p2.yVal + p2.yVal;
//	return p;
//}
ostream& operator<<(ostream& o, const Point2D& p) {
	o << "Gia tri cua x la: " << p.xVal << endl << "Gia tri cua y la: " << p.yVal << endl;
	return o;
}
istream& operator>>(istream& i, Point2D& p) {
	cout << "xVal= ";
	i >> p.xVal;
	cout << "yVal= ";
	i >> p.yVal;
	return i;

}