#include "Vector.h"
int& Vector::operator[] (const int& i) {
	static int n = 0;
	return (i > 0 && i < this->size) ? *(this->data + i) : n;
}
Vector::Vector(int s, int t) {
	this->size = s;
	this->data = new int[this->size];
	for (int i = 0; i < s; i++) {
		//*(this->data + i) = t;
		(*this)[i] = t;
	}
}
Vector::~Vector() {
	delete[] this->data;
}
void Show(const Vector& p) {
	for (int i = 0; i < p.size; i++) {
		cout << *(p.data + i) << " ";
	}
}