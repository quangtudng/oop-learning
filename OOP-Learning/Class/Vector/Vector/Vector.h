#pragma once
#include <iostream>
using namespace std;
class Vector
{
public:
	int* data;
	int size;
public:
	Vector(int s = 3, int t = 2);
	~Vector();
	friend void Show(const Vector&);
	int& operator[] (const int& i);
};

