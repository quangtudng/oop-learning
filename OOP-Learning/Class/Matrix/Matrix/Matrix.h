#pragma once
#include <iostream>
using namespace std;
class Matrix
{
public:
	int** data;
	int row, col;
	Matrix(int = 2, int = 3, int = 1);
	Matrix(Matrix&);
	~Matrix();
	friend ostream& operator<<(ostream&, const Matrix&);
	int& operator()(const int&, const int&);
};

