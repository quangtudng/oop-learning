﻿#include "Matrix.h"
Matrix::Matrix(int r, int c, int t) : row(r), col(c) {
	this->data = new int* [this->row];
	for (int i = 0; i < this->row; i++) {
		*(this->data + i) = new int[this->col];
		for (int j = 0; j < this->col; j++) {
			// Toán tử lấy giá trị nội dung
			*(*(this->data + i) + j) = t;
			//this->data[i][j] = t;
		}
	}
}
Matrix::Matrix(Matrix& p) : row(p.row), col(p.col) {
	for (int i = 0; i < this->row; i++) {
		for (int j = 0; j < this->col; j++) {
			*(*(this->data + j)) = *(*(p.data + j));
		}
	}
}
Matrix::~Matrix() {
	for (int i = 0; i < this->row; i++) {
		delete* (this->data + i);
	}
	delete this->data;
}
ostream& operator<<(ostream& o, const Matrix& p) {
	for (int i = 0; i < p.row; i++) {
		for (int j = 0; j < p.col; j++) {
			o << *(*(p.data + i) + j) << " ";
		}
		o << endl;
	}
	return o;
}
int& Matrix::operator()(const int& i, const int& j) {
	static int n = 0;
	return (i < row && j < col) ? *(*(this->data + i) + j) : n;
}