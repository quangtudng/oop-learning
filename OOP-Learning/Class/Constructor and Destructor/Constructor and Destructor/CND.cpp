#include "CND.h"
CND::CND(const int size) {
	cout << "Default constructor called" << endl;
	this->card = size;
}
CND::~CND() {
	cout << "Destructor called" << endl;
	delete[] element;
}
CND::CND(const CND& s)
{
	*this = s;
	cout << "Constructor copy called" << endl;
}
CND CND::testFunc1(CND s1)
{
	CND* s = new CND(50);
	return *s;
}
CND CND::testFunc2(CND s2)
{
	CND s(10);
	return s;
}