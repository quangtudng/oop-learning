#include <iostream>
using namespace std;
class CND
{
private:
	int* element;
	int card;
public:
	CND(const int size);
	CND(const CND&);
	~CND();
	CND testFunc1(CND);
	CND testFunc2(CND);
};