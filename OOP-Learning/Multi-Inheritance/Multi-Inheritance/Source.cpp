﻿#include "Point3D.h"
int main() {
	cout << "Ham dung cua lop cha se duoc goi truoc lop con: " << endl;
	// Lớp con đa thừa kế
	cout << "______________________________________________________" << endl;
	Point3D p3(6,7,8,9,10);
	cout << "______________________________________________________" << endl;
	p3.Point2D::Show();
	cout << "______________________________________________________" << endl;
	p3.Point2DD::Show();
	cout << "______________________________________________________" << endl;
	p3.Point3D::Show();
	cout << "______________________________________________________" << endl;
	return 0;
}
// Lưu ý : Trong đa thừa kế nếu lớp con kế thừa từ nhiều lớp cha một phương thức
// Nếu lớp con không override và nếu không gọi thông qua toán tử định phạm vi sẽ bị báo lỗi biên dịch mơ hồ