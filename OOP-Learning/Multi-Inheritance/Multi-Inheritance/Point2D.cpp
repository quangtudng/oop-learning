#include "Point2D.h"
int Point2D::count = 0;
// Default constructor with variable
Point2D::Point2D(int xVal, int yVal) : xVal(xVal), yVal(yVal) {
	cout << "Ham dung day du tham so cua Point2D duoc goi" << endl;
	Point2D::count++;
}
// Destructor
Point2D::~Point2D() {
	cout << "Ham huy Point2D" << endl;
	Point2D::count--;
}
// Other methods
void Point2D::Show() {
	cout << "Ham show Point2D" << endl;
	cout << "Gia tri cua x la: " << this->xVal << endl << "Gia tri cua y la: " << this->yVal << endl;
}
