﻿#pragma once
#include <iostream>
using namespace std;
class Point2D
{
protected:
	int xVal;
	int yVal;
	// Biến count 
	static int count;
public:
	Point2D(int = 6, int = 9);
	~Point2D();
	// Để cắt liên kết ( liên kết động ) , ta biến nó thành hàm ảo => cắt sợi dây liên kết
	void Show();
};
