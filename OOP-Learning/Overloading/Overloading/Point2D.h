﻿#pragma once
#include <iostream>
using namespace std;
class Point2D
{
	private:
		int xVal;
		int yVal;
	public:
		// -- Constructor --
		// Hàm dựng mặc đinh không có tham số
		Point2D();
		// Hàm dựng mặc định với 2 tham số
		Point2D(int, int);
		// Hàm dựng được tạo với mục đích chuyển đổi tham số chương trình định nghĩa thành tham số do user định nghĩa
		Point2D(int);
		// Hàm dựng sao chép, phép gán có thể coi là hàm dựng sao chép , nên không cần đa năng hóa phép gán
		Point2D(const Point2D&);
		// -- Destructor --
		~Point2D();
		// -- Other methods --
		// Show the variables
		void Show();
		// Hàm đa năng hóa
		friend istream& operator>> (istream&, Point2D&); // Toán tử nhập ( >> )
		friend ostream& operator<< (ostream&, const Point2D&); // Toán tử xuất ( << )
		Point2D operator+ (Point2D&); // Toán tử cộng ( + )
		Point2D operator+ (); // Toán tử dương đơn hạng ( + )
		Point2D operator- (Point2D&); // Toán tử trừ ( - )
		Point2D operator- (); // Toán tử âm đơn hạng ( - )
		Point2D operator* (Point2D&); // Toán tử nhân ( * )
		Point2D operator/ (Point2D&); // Toán tử chia ( / )
		Point2D operator! (); // Toán tử Not ( ! )
		Point2D operator~ (); // Toán tử đảo bit ( ~ )
		Point2D operator& (Point2D&); // Toán tử And ( & )
		Point2D operator++ (); // Toán tử tăng tiền tố ( ++i )
		Point2D operator++ (int); // Toán tử tăng hậu tố ( i++ )
		Point2D operator-- (); // Toán tử giảm tiền tố ( ++i )
		Point2D operator-- (int); // Toán tử giảm hậu tố ( i++ )
		Point2D operator() (int, int);
};
// Lưu ý : Các toán tử () , [] , -> , phải được đa năng bởi một hàm thành viên không tĩnh