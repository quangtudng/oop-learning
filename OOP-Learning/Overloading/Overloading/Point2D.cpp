#include "Point2D.h"
Point2D::Point2D()
	: xVal(1), yVal(2)
{

}
Point2D::Point2D(int xVal, int yVal)
	: xVal(xVal), yVal(yVal)
{

}
Point2D::Point2D(int n) : xVal(n), yVal(n) {

}

Point2D::Point2D(const Point2D& p) {
	this->xVal = p.xVal;
	this->yVal = p.yVal;
}
// Destructor
Point2D::~Point2D() {

}
// Other methods
void Point2D::Show() {
	cout << "Ham show cua POint2D duoc goi" << endl;
	cout << "Gia tri cua x la: " << this->xVal << endl;
	cout << "Gia tri cua y la: " << this->yVal << endl;
}
ostream& operator<< (ostream& output, const Point2D& Object) {
	cout << "Doi tuong co cac thong so la: " << endl;
	output << "Gia tri cua xVal: " << Object.xVal << endl;
	output << "Gia tri cua yVal: " << Object.yVal << endl;
	return output;
}
istream& operator>> (istream& input, Point2D& Object) {
	cout << "Nhap vao thong tin cua doi tuong: " << endl;
	cout << "Nhap gia tri cua xVal: ";
	input >> Object.xVal;
	cout << "Nhap gia tri cua yVal: "; 
	input >> Object.yVal;
	return input;
}
Point2D Point2D::operator+(Point2D& Object) {
	return Point2D(this->xVal + Object.xVal, this->yVal + Object.yVal);
}
Point2D Point2D::operator+ () {
	return Point2D(this->xVal, this->yVal);
}
Point2D Point2D::operator-(Point2D& Object) {
	return Point2D(this->xVal - Object.xVal, this->yVal - Object.yVal);
}
Point2D Point2D::operator- () {
	return Point2D(-this->xVal, -this->yVal);
}
Point2D Point2D::operator*(Point2D& Object) {
	return Point2D(this->xVal * Object.xVal, this->yVal * Object.yVal);
}
Point2D Point2D::operator/(Point2D& Object) {
	if (Object.xVal != 0 && Object.yVal !=0) {
		return Point2D(this->xVal / Object.xVal, this->yVal / Object.yVal);
	}
	else {
		cout << "Phep chia bi sai" << endl;
	}
}
Point2D Point2D::operator!() {
	return Point2D(!this->xVal, !this->yVal);
}
Point2D Point2D::operator~() {
	return Point2D(~this->xVal, ~this->yVal);
}
Point2D Point2D::operator&(Point2D& Object) {
	return Point2D(this->xVal & Object.xVal, this->yVal & Object.yVal);
}
Point2D Point2D::operator++ () {
	this->xVal++;
	this->yVal++;
	return *this;
}
Point2D Point2D::operator++(int) {
	Point2D Before(this->xVal, this->yVal);
	this->xVal++;
	this->yVal++;
	return Before;
}
Point2D Point2D::operator-- () {
	this->xVal--;
	this->yVal--;
	return *this;
}
Point2D Point2D::operator-- (int) {
	Point2D Before(this->xVal, this->yVal);
	this->xVal--;
	this->yVal--;
	return Before;
}
Point2D Point2D::operator() (int x,int y) {
	this->xVal = x;
	this->yVal = y;
	return *this;
}