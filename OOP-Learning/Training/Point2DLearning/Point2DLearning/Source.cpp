﻿#include <iostream>
#include "Point2D.h"
using namespace std;
int main() {
	// Trường hợp 1 : Đối tượng thứ nhất không có tham số truyền vào 
	Point2D p1;
	cout << "1) Doi tuong thu nhat khong co tham so truyen vao" << endl;
	p1.Show();
	DisplayFriend(p1);
	// Trường hợp 2 : Đối tượng thứ hai có 1 tham số truyền vào
	Point2D p2(1);
	cout << "2) Doi tuong thu hai co mot tham so truyen vao" << endl;
	p2.Show();	
	// Trường hợp 3 : Đối tượng thứ ba có 2 tham số truyền vào
	Point2D p3(3, 4);
	cout << "3) Doi tuong thu ba co hai tham so truyen vao" << endl;
	p3.Show();
	// Trường hợp 4 : Đối tượng thứ tư được sao chép từ đối tượng thứ 3
	cout << "4) Doi tuong thu tu duoc sao chep tu doi tuong thu ba: " << endl;
	Point2D p4(p3);
	p4.Show();
	p4.showCount();
	// Trường hợp 5 : Đối tượng thứ tư được xuất ra bằng toán tử xuất đã được đa năng hóa
	Point2D p5;
	cout << p5;
	system("pause");
	return 0;
}