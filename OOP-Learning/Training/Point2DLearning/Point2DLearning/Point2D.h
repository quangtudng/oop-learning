﻿#pragma once
#include <iostream>
using namespace std;
class Point2D
{
	private:
		int x;
		int y;
		static int count;
	public:
		// Hàm dựng mặc định
		Point2D();
		Point2D(int);
		Point2D(int, int);
		// Hàm dựng sao chép 
			// const : Chỉ sao chép chứ không thay đổi 
			// Point2D& : Sử dụng tham chiếu để sử dụng chung vùng nhớ
		Point2D(const Point2D&);
		// Hàm hủy
		~Point2D();
		// Hàm thành viên
		void Show();
		// Hàm thành viên tĩnh
		static void showCount();
		// Hàm bạn sử dụng để biểu diễn thuộc tính
			// const : Chỉ sao chép chứ không thay đổi 
			// & : Sử dụng tham chiếu để sử dụng chung vùng nhớ
		friend void DisplayFriend(const Point2D&);
		// Hàm bạn sử dụng để đa năng hóa lại toán tử xuất
		friend ostream& operator<<(ostream&, const Point2D&);
};

