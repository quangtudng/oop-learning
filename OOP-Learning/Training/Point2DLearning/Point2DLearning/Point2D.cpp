﻿#include "Point2D.h"
// Hàm dựng mặc định
int Point2D::count = 0;
Point2D::Point2D() : x(0), y(0) {
	Point2D::count++;
}

Point2D::Point2D(int xVal) : x(xVal), y(xVal) {
	Point2D::count++;
}

Point2D::Point2D(int xVal, int yVal) : x(xVal), y(yVal) {
	Point2D::count++;
}
// Hàm dựng sao chép
Point2D::Point2D(const  Point2D& p2) {
	this->x = p2.x;
	this->y = p2.y;
	Point2D::count++;
}
// Hàm hủy
Point2D::~Point2D() {
	count--;
	cout << "Destructor called!";
}
// Hàm thành viên 
void Point2D::Show() {
	cout << "Value of x: " << this->x << endl << "Value of y: " << this->y << endl;
}
// Hàm thành viên tĩnh 
void Point2D::showCount() {
	if (Point2D::count > 1)	{
		cout << "Constructors have been called " << Point2D::count << " times" << endl;
	}
	else {
		cout << "Constructors have been called " << Point2D::count << " time" << endl;
	}
}
// Hàm bạn biểu diễn các thuộc tính
void DisplayFriend(const Point2D &p) {
	cout << "Friend Function: " << endl;
	cout << "Value of x: " << p.x << endl << "Value of y: " << p.y << endl;
}
ostream& operator<<(ostream& o, const Point2D& p) {
	o << "Value of x : " << p.x << endl;
	o << "Value of y : " << p.y << endl;
	return o;
}