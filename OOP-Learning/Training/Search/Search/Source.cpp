#include <iostream>
using namespace std;

void Nhap(int* p ,int n) {
	cout << "Nhap vao cac phan tu: " << endl;
	for (int i = 0; i < n; i++) {
		cout << "p["  << i << "]=";
		cin >> *(p + i);
	}
}
void Xuat(int* p, int n) {
	cout << "Cac phan tu ban vua nhap vao la: " << endl;
	for (int i = 0; i < n; i++) {
		cout << "p[" << i << "]=";
		cout << *(p + i) << endl;
	}
}
// Linear Search
void LinearSearch(int* p, int n, int x) {
	int found = 0;
	for (int i = 0; i < n; i++) {
		if (*(p + i) == x) {
			found = i;
			break;
		}
	}
	if (found != 0) {
		cout << "Tim thay so " << x << "tai vi tri " << found + 1 << endl;
	}
	else {
		cout << "Khong tim thay so " << x << endl;
	}
}
// 
int main() {
	int n, x;
	cout << "Nhap vao so phan tu: ";
	cin >> n;
	int* p = new int[n];
	Nhap(p, n);
	Xuat(p, n);
	cout << "Nhap vao so can tim: ";
	cin >> x;
	LinearSearch(p, n, x);
	delete []p;
	return 0;
}