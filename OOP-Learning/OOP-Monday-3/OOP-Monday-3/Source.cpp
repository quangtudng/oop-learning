﻿#include <iostream>
using namespace std;
// Hàm
void Swap(int& a, int& b) {
	a++;
	b++;
}
// Khuôn mẫu hàm
template<typename T>
void Swap(T& a, T& b) {
	T temp = a;
	a = b;
	b = temp;
}
// Template chỉ có tác dụng với hàm ngay sau nó
int main() {
	int m = 1, n = 2;
	int a = 6, b = 9;
	// Cách gọi tường minh ( Đi tìm khuôn mẫu hàm trước )
	Swap<int>(a, b);
// 1.Tìm khuôn mẫu hàm có tên swap
// 2.Tìm khuôn mẫu hàm có 2 tham số
// 3.Tìm khuôn mẫu hàm có cùng kiểu dữ liệu
// 4.Từ khuôn mẫu hàm nó tạo ra một hàm Swap int 
// 5.Truyền m,n
	// Cách gọi không tường minh ( Đi tìm hàm trước )
	Swap(m, n);
// 1.Tìm hàm có tên swap ( nếu có sẽ thực hiện trước )
	cout <<a << endl << b << endl << m << endl << n;
	return 0;
}