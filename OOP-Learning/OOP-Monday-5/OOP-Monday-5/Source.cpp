﻿#include <iostream>
#include <list>
#include <string>
using namespace std;
double MyNigga(double Tu, double Mau) throw(int) {
	if (Mau == 0) {
		throw invalid_argument("My nigga, you are stupid");
	}
	else {
		return Tu / Mau;
	}
}
int main() {
	double Tu, Mau, KetQua;
	bool check;
	do
	{
		check = true;

		cout << "Nhap vao tu: ";
		cin >> Tu;
		cin.ignore(1);
		cout << "Nhap vao mau: ";
		cin >> Mau;
		cin.ignore(1);
		try {
			KetQua = MyNigga(Tu, Mau);
			cout << "Ket qua la: " << KetQua;
		}
		catch (invalid_argument& error) {
			cout << error.what() << endl;
			check = false;
		}
		catch (...) {
			cout << "Yowtf" << endl;
			check = false;
		}
	} while (check == false);

}
// Khi ngoại lệ là một đối tượng thì khi thực hiện xong hàm hủy sẽ được gọi , tuy nhiên nếu là con trỏ thì không, phải giải phóng con trỏ
// Khi đa hình trong lúc kế thừa , chỉ có thể giảm ngoại lệ chứ không được phép tăng 
// Ôn tập : biến tham chiếu( dùng chung vùng nhớ  , phải gán ) , biến con trỏ ( heap = địa chỉ vùng stack + stack = giá trị con trỏ )
// Ôn tập : Mảng một chiều , hai chiều 
// Ôn tập : Các tính chất hướng đối tượng
// Ôn tập : Đa năng hóa toán tử
// Ôn tập : Đa hình và kế thừa ( hàm dựng + hàm hủy không kế thừa , sẽ có thứ tự gọi )
// Ôn tập : Bổ sung truy cập kế thừa , lớp cơ sở ảo
// Ôn tập : Liên kết động và liên kết tĩnh
// Ôn tập : Lớp trừu tượng ( không được phép khai báo đối tượng thuộc lớp này ) và lớp con kế thừa sẽ cùng là lớp trừu tượng ( và ta phải overide )
// Ôn tập : Upcast, downcast, slicing
// Ôn tập : Template ( khuôn mẫu hàm + khuôn mẫu lớp )
// Ôn tập : Exception
