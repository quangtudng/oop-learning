﻿#include "Point3D.h"
#include "Point3DD.h"
int main() {
	// Cha
	//Point2D p2(6, 7);
	// Con
	Point3D p3(8, 9, 10);
	// Cha có thể trỏ tới con - Upcast
	//Point2D* p2D = &p2;
	////Point2D* p2D = &p3;

	////p3.Show();
	// Cha không thể trỏ tới những thuộc tính đặc trưng của lớp con!
	// p2D->Display();
	// Liên quan tới Đa hình động và đa hình tĩnh !
	////p2D->Show();

	// Con không thể trỏ về cha - Downcast
	//Point3D* p3D = &p2;
	// Ép kiểu lớp
	// Nó sẽ ưu tiên gọi hàm kiểu dữ liệu con trỏ ( LIÊN KẾT TĨNH ) => KHÔNG LOGIC !  Nếu không có hàm nó mới tới đối tượng đang trỏ tìm hàm
	// => Phải cắt liên kết của con trỏ này về hàm của kiểu dữ liệu con trỏ 
	// Giải pháp : Chỉ xây một liên kết di động khi gọi hàm ( LIÊN KẾT ĐỘNG )
	////Point3D* p3D = static_cast<Point3D *>(&p2);
	////p2.Show();
	////p3D->Show();

	// Slicing
	////Point2D p4;
	////p4 = p3;
	////p4.Show();

	// Đa thừa kế
	//Point3DD p3DD;
	//p3DD.Show();	
	//p3DD.Point2D::Show();
	//p3DD.Point2DD::Show();

	//Point2D* p2d;
	//p2d = &p3;
	// Sau khi thêm từ khóa virtual , thì nó chạy logic
	// Chỉ dùng virtual khi nào cần override và cả cha và con đều cần từ khóa virtual
	//p2d->Show();

	Point2D* p2D = new Point3D(6, 9, 6);
	delete p2D;
	// Chỉ delete khi có new
	return 0;
}
// Kế thừa : Trong khi kế thừa , hàm dựng của lớp cha sẽ không được kế thừa , tuy nhiên lớp con vẫn sẽ gọi hàm dựng của lớp cha
// Hàm dựng KHÔNG đặt virtual, tuy nhiên hàm hủy thì nên đặt virtual
// Virutal 2D : Đúng nếu có 2 lớp như thế này , nó gọi hàm hủy con , sau đó nó gọi về cha . Nhưng Sai nếu có 2D , 3D , 4D
// Virutal 2D - Virtual 3D : Cắt liên kết cả 2 hàm , cả 2 hàm đều không thông , nó sẽ ưu tiên gọi về hàm hủy thằng nó đang trỏ tới ( logic )
// Nếu khi đa hình thì Overload sẽ không kế thừa các method đã được overload mà chỉ kế thừa lại hàm mà mình vừa đa hình

// Abstract class 
// Lớp trừu tượng: Không đối tượng nào thuộc lớp này vì các đối tượng không có cùng chung phương thức hay thuộc tính
// Công dụng : Chỉ dùng để quản lý + bao lại để giảm thiểu số lượng phương thức trên ứng dụng có nhiều lớp có phương thức giống nhau (Java : Interface : Phương thức thuần túy , toàn bộ phương thức phải là thuần ảo , C++ : Phương thức thuần ảo )
// Về bản chất, phương thức thuần ảo trong lớp trừu tượng chỉ được phép khai báo không được định nghĩa 
// Khai báo thuần ảo virtual void Show()=0;
// Lớp gọi là một lớp trừu tượng khi bên trong nó chứa đựng ít nhất một phương thức thuần ảo ( trừu tượng )
// Khi lớp con kế thừa từ lớp trừu tượng , nếu không override lại hàm thuần ảo, lớp con cũng sẽ là một lớp trừu tượng
// Ví dụ : Lớp động vật + lớp thực vật
