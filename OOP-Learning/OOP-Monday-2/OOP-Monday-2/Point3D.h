﻿#pragma once
#include "Point2D.h"
class Point3D :
	public Point2D
{
	int zVal;
	public:
		Point3D(int);
		Point3D(int, int, int);
		virtual ~Point3D();
		void Display();
		// Overriding - Đa hình ( Tái định nghĩa )
		// Cứ thêm vào virtual ở thằng con , không thừa
		virtual void Show();
};
