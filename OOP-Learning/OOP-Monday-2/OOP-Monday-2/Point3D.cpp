﻿#include "Point3D.h"
// Gọi hàm dựng lớp cha chỉ có thể thông qua danh sách khởi tạo thành viên!
// Các thuộc tính kế thừa chỉ có thể khởi tạo thông qua hàm dựng
Point3D::Point3D(int x, int y, int z) 
	: Point2D(x,y) , zVal(z)
{
	
}
Point3D::Point3D(int z)
	: Point2D() , zVal(z)
{

}
// Tái định nghĩa lại hàm Show
void Point3D::Show() {
	cout << "Gia tri cua x la: " << this->xVal << endl << "Gia tri cua y la: " << this->yVal << endl << "Gia tri cua z la: " << this->zVal;
}
void Point3D::Display() {
	cout << "Display:" << endl;
	cout << this->xVal << " " << this->yVal << " " << this->zVal;
}
Point3D::~Point3D() {
	cout << "Yo dog , Point3D is gone" << endl;
}
// Khi dựng nó sẽ gọi hàm dựng lớp cha trước
// Khi hủy nó sẽ gọi hàm hủy của lớp con trước