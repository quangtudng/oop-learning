﻿#include "Point2D.h"
int Point2D::count = 0;
// Default constructor with variable
Point2D::Point2D(int xVal, int yVal) : xVal(xVal), yVal(yVal) {
	count++;
}
// Destructor
Point2D::~Point2D() {
	cout << "Yo dog, Point2D is gone" << endl;
	Point2D::count--;
}
// Other methods
void Point2D::Show() {
	cout << "Point2D" << endl;
	cout << "Gia tri cua x la: " << this->xVal << endl << "Gia tri cua y la: " << this->yVal << endl;
}
ostream& operator<<(ostream& o, const Point2D& p) {
	o << "Gia tri cua x la: " << p.xVal << endl << "Gia tri cua y la: " << p.yVal << endl;
	return o;
}