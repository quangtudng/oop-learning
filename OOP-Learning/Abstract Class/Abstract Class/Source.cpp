﻿#include "Point3D.h"
int main() {
	// Không thể khai báo
	//Point2D p2;
	Point3D p3(1,2,3);
	p3.Show();
	return 0;
}
// Abstract class 
// Lớp trừu tượng: Không đối tượng nào thuộc lớp này vì các đối tượng không có cùng chung phương thức hay thuộc tính
// Công dụng : Chỉ dùng để quản lý + bao lại để giảm thiểu số lượng phương thức trên ứng dụng có nhiều lớp có phương thức giống nhau (Java : Interface : Phương thức thuần túy , toàn bộ phương thức phải là thuần ảo , C++ : Phương thức thuần ảo )
// Về bản chất, phương thức thuần ảo trong lớp trừu tượng chỉ được phép khai báo không được định nghĩa 
// Khai báo thuần ảo virtual void Show()=0;
// Lớp gọi là một lớp trừu tượng khi bên trong nó chứa đựng ít nhất một phương thức thuần ảo ( trừu tượng )
// Khi lớp con kế thừa từ lớp trừu tượng , nếu không override lại hàm thuần ảo, lớp con cũng sẽ là một lớp trừu tượng
// Ví dụ : Lớp động vật + lớp thực vật
