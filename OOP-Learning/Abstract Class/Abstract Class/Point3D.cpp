﻿#include "Point3D.h"
// Gọi hàm dựng lớp cha chỉ có thể thông qua danh sách khởi tạo thành viên!
// Các thuộc tính kế thừa chỉ có thể khởi tạo thông qua hàm dựng
Point3D::Point3D(int x, int y, int z)
	: Point2D(x, y), zVal(z)
{

}
Point3D::Point3D(int z)
	: Point2D(), zVal(z)
{

}
// Tái định nghĩa lại hàm Show
void Point3D::Show() {
	cout << "Gia tri cua x la: " << this->xVal << endl << "Gia tri cua y la: " << this->yVal << endl << "Gia tri cua z la: " << this->zVal;
}

Point3D::~Point3D() {
	cout << "Ham huy cua Point3D duoc goi" << endl;
}