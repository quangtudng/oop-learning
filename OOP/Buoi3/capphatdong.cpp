#include<iostream>
using namespace std;
int main() {
    // Cap phat tinh 
    int x=1; 
    int *p1=&x;
    // Cap phat dong , vung nho nay chi co the truy cap bang con tro p2
    int *p2 = new int; 
    *p2 =2;

    cout << x; // 1
    cout << *p1;// 1
    cout << *p2;// 2
    p1=p2;
    p2= new int; *p2 = 3;
    cout << *p1;// 2
    cout << *p2;//3
    delete p2;
    return 0;
}