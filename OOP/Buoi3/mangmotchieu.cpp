#include <iostream>
using namespace std;
void swap( int &x,int &y) {
    int temp =x;
    x=y;
    y=temp;
}
void nhap(int *a , int n) {
    for(int i=0 ; i<n; i++) {
        cin >> *(a+i);
    }
}
void xuat(int *a , int n) {
    for(int i=0; i<n; i++) {
        cout << *(a+i);
    }
    cout<< endl;
}
bool ascending(int a, int b) {
    return a>b;
}
bool descending(int a , int b) {
    return b<a;
}
void sort(int *a , int n , bool (*compFunc)(int, int)) {
    for(int i=0; i<n-1; i++) {
        for(int j=i+1;j<n; j++) {
            if(compFunc(*(a+i), *(a+j))) {
                swap(*(a+i), *(a+j));
            }
        }
    }
}
int main() {
    int n;
    cout << "Nhap n: ";
    cin >> n;
    int *p= new int[n];
    nhap(p,n);
    xuat(p,n);
    // sort(p, n, ascending);
    // sort(p,n, descending);
    xuat(p,n);
    delete p;
}