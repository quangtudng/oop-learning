#include <iostream>
using namespace std;
void nhap(int **A , int m, int n) {
    for(int i=0 ; i<m ; i++) {
        for(int j=0; i<=n;j++) {
            cin >> *(((*(A+i))) +j);
        }
    }
}
void xuat(int **A, int m,int n) {
    for(int i=0; i<m;i++) {
        for(int j=0;j<n;j++) {
            cout << *(((*(A+i))) +j);
        }
    }
}
int main() {
    int **A;
    int m,n;
    cout << "Nhap m: ";
    cin >> m;
    cout << "Nhap n: ";
    cin >> n;
    
    A= new int *[m];
    for(int i=0; i<m;i++) {
        A[i] = new int[n];
    }

    nhap(A, m ,n);
    xuat(A, m ,n);
    for(int i=0;i<m ;i++) {
        delete []A[i];
    } 
    delete []A;
    return 0;
}