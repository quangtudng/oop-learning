#include <iostream>
using namespace std;

// Gán 2 con trỏ phải cùng kiểu dữ liệu
int main() {
    int x = 1;
    int *p1 , *p2;
    p1 = &x;
    cout << *p1 << endl; // p1 -> 1
    *p1 +=2; // p1 -> 3
    cout << x << endl; //  p1
    p2=p1; // p2 -> 3
    *p2 += 3; // p2 -> 6
    cout << x << endl; // p2
    return 0;
}