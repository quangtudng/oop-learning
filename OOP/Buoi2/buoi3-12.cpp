#include <iostream>
using namespace std;
typedef int P;
typedef int* Q;
int main() {
    int x=1, y=2;
    P *p1, *p2;
    p1 = &x;
    p2 = &y;
    cout << "p1 = " << *p1 << " " << endl << "p2 = " << *p2 << endl;
    
    Q p3, p4;
    p3 = &x;
    p4 = &y;

    cout << "p3 = " << *p3 << " " << endl << "p4 = " << *p4 << endl;

    // P* p5, p6;
    // p5 = & x;
    // p6 = & y;
    return 0;
}