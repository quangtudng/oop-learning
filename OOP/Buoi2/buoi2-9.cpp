#include<iostream>
using namespace std;

// Trong con trỏ , vùng nhớ bên Heap để lưu địa chỉ và vùng nhớ bên Stack để lưu giá trị nội dung
// khi khai báo int *q , con trỏ này có 2 vùng nhớ , trong đó vùng nhớ bên Stack là một giá trị mặc định , bên heap sẽ là NULL 
// int *q = &x -> con trỏ q đang trỏ tới vùng nhớ của biến x , *p là nội dung vùng nhớ Stack đang trỏ , p là nội dung vùng nhớ Heap ( địa chỉ )

int main() {
    int x =10, y=20;
    int *p1= &x, *p2=&y;

    // p1 = & x;
    // p2 = & y;
    cout <<"x= " << x << endl;
    cout <<"y= " << y << endl ;
    cout <<"Stack Value : *p1= " << *p1 << endl;
    cout <<"Stack Value : *p2= " << *p2 << endl;

    *p1 = 50;
    *p2 = 90;
    cout <<"x= " << x << endl;
    cout <<"y= " << y << endl ;
    cout <<"Stack Value : *p1= " << *p1 << endl;
    cout <<"Stack Value : *p2= " << *p2 << endl;

    *p1 = *p2;
    cout <<"x= " << x << endl;
    cout <<"y= " << y << endl ;
    cout <<"Stack Value : *p1= " << *p1 << endl;
    cout <<"Stack Value : *p2= " << *p2 << endl;

    return 0;
}

