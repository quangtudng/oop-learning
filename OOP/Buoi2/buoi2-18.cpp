#include<iostream>
using namespace std;
void Swap(int &x, int &y) {
    int temp = x;
    x= y;
    y= temp;
}
int main() {
    int m =5 , n =10;
    void(*pSwap) (int &, int &) = Swap; // *p = &x , ở đây nếu ghi Swap thì ta đã nhận được địa chỉ của hàm Swap
    (*pSwap)(m,n);
    cout << m << endl << n;
    return 0;
}
// Con trỏ hàm
// Bài tập  : Viết hàm sort