#include <iostream>
using namespace std;

void Nhap(int *A, int n) {
    for(int i=0; i< n; i++) {
        cin >> *(A+i);
    }
}
void Xuat(int *A,int n) {
    for(int i=0; i<n; i++) {
        cout << *(A+i);
    }
}
void MinMax(int *A, int n, int &min,int &max) {
    min = max= *A;
    for(int i=0; i<n-1; i++ ) {
        for(int j=i+1; j<n; j++) {
            if(*A < *(A+j)) {
                max = *(A+j);
            }
        }
    }
    cout << max;
    for(int i=n; i>0; i-- ) {
        for(int j=n-1; j>0; j++) {
            if(*A < *(A+j)) {
                max = *(A+j);
            }
        }
    }
    cout << max;
}
int main() {
    int n, A[50];
    int min,max;
    cout << "Nhap vao so luong: ";
    cin >> n;
    Nhap(A,n);
    Xuat(A,n);
    MinMax(A,n,min,max);
    return 0;
}