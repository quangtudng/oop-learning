#include<iostream>
using namespace std;

int &Func() {
    int x = 1;
    return x; // Dù vùng nhớ đã bị thu hồi , nó vẫn trỏ tới vùng nhớ đã được giải phóng
}
int main() {
    int *p = &Func();
    cout << *p; // 1
    cout << *p; // 0 - Sau lần thứ nhất , con trỏ nhìn tới vùng nhớ của Func đã bị reset
    return 0;
}