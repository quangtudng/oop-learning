#include<iostream>
using namespace std;
// Con trỏ hằng const int *p = &x , không cho phép thay đổi giá trị vùng nhớ nhưng có thể thay đổi vùng nhớ nó trỏ tới
int main() {
    int x=9,y=10;
    const int *p = &x;
    p = &y;
    cout << *p;
}
// Hằng con trỏ int* const p = &x , có thể thay dổi giá trị nó trỏ tới nhưng không cho phép thay đổi vùng nhớ trỏ tới
int main() {
    int x=9 , y=10;
}