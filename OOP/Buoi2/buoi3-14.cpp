#include <iostream>
using namespace std;

// A[2][3] hàng 2 cột 3
// A[i] = *(A+i)

int main() {
    int A[] = {1, 2, 3, 4, 5};
    int *p = A;
    p+=2;
    cout << *p; // A[2] = 3
    (p)--;
    cout << *p;
    (*p)++;
    cout<< *p;
    return 0;
}