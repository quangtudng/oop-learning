﻿#include "Vector.h"
#include <iostream>
using namespace std;
Vector::Vector(int s,int t) {
	this->size = s;
	this->data = new int[this->size];
	for (int i = 0; i < s; i++) {
		*(this->data+i)= t;
		// Cách ghi ở đây ít dùng
		//this->data[i] = t;
	}
}
Vector::~Vector() {
	delete[] this->data;
}
void Show(const Vector& p) {
	cout << "Co tat ca " << p.size << " phan tu";
	for (int i = 0; i < p.size; i++) {
		cout << *(p.data + i) << endl;
	}
}